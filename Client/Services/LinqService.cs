using System;
using System.Net;
using System.Threading.Tasks;

namespace Client.Services
{
    public static class LinqService
    {
        private static readonly WebClient Client = new WebClient();

        private static string _path = "https://localhost:5001/api";

        public static async Task<string> GetTask1(int id)
        {
            return await Client.DownloadStringTaskAsync(new Uri($"{_path}/linq/task1?id={id}"));
        }

        public static async Task<string> GetTask2(int id)
        {
            return await Client.DownloadStringTaskAsync(new Uri($"{_path}/linq/task2?id={id}"));
        }

        public static async Task<string> GetTask3(int id)
        {
            return await Client.DownloadStringTaskAsync(new Uri($"{_path}/linq/task3?id={id}"));
        }

        public static async Task<string> GetTask4()
        {
            return await Client.DownloadStringTaskAsync(new Uri($"{_path}/linq/task4"));
        }

        public static async Task<string> GetTask5()
        {
            return await Client.DownloadStringTaskAsync(new Uri($"{_path}/linq/task5"));
        }

        public static async Task<string> GetTask6(int id)
        {
            return await Client.DownloadStringTaskAsync(new Uri($"{_path}/linq/task6?id={id}"));
        }

        public static async Task<string> GetTask7(int id)
        {
            return await Client.DownloadStringTaskAsync(new Uri($"{_path}/linq/task7?id={id}"));
        }
    }
}