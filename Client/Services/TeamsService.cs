using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Models;
using Newtonsoft.Json;

namespace Client.Services
{
    public class TeamsService : IService<Team>
    {
        private static readonly WebClient Client = new WebClient();

        private static string _path = "https://localhost:5001/api";

        public async Task<Team> GetAsync(int id)
        {
            var str = await Client.DownloadStringTaskAsync(new Uri($"{_path}/Teams/{id}"));
            return JsonConvert.DeserializeObject<Team>(str);        }

        public async Task AddAsync(Team entity)
        {
            await Utils.Request($"{_path}/Teams", "POST", JsonConvert.SerializeObject(entity));
        }

        public async Task<IEnumerable<Team>> GetAllAsync()
        {
            var str = await Client.DownloadStringTaskAsync(new Uri($"{_path}/Teams"));
            return JsonConvert.DeserializeObject<IEnumerable<Team>>(str);        }

        public async Task DeleteAsync(int id)
        {
            await Utils.Request($"{_path}/Teams/{id}", "DELETE");
        }

        public async Task UpdateAsync(int id, Team entity)
        {
            await Utils.Request($"{_path}/Teams/{id}", "PUT", JsonConvert.SerializeObject(entity));
        }
    }
}