using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Models;
using Newtonsoft.Json;

namespace Client.Services
{
    public class ProjectService : IService<Project>
    {
        private static readonly WebClient Client = new WebClient();

        private static string _path = "https://localhost:5001/api";

        public async Task<Project> GetAsync(int id)
        {
            var str = await Client.DownloadStringTaskAsync(new Uri($"{_path}/Projects/{id}"));
            return JsonConvert.DeserializeObject<Project>(str);
        }

        public async Task AddAsync(Project entity)
        {
            await Utils.Request($"{_path}/Projects", "POST", JsonConvert.SerializeObject(entity));
        }

        public async Task<IEnumerable<Project>> GetAllAsync()
        {
            var str = await Client.DownloadStringTaskAsync(new Uri($"{_path}/Projects"));
            return JsonConvert.DeserializeObject<IEnumerable<Project>>(str);
        }

        public async Task DeleteAsync(int id)
        {
            await Utils.Request($"{_path}/Projects/{id}", "DELETE");
        }

        public async Task UpdateAsync(int id, Project entity)
        {
            await Utils.Request($"{_path}/Projects/{id}", "PUT", JsonConvert.SerializeObject(entity));
        }
    }
}