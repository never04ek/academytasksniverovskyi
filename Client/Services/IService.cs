using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client.Services
{
    public interface IService<T> where T:class
    {
        Task<T> GetAsync(int id);
        Task AddAsync(T entity);
        Task<IEnumerable<T>> GetAllAsync();
        Task DeleteAsync(int id);
        Task UpdateAsync(int id, T entity);
    }
}