using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Models;
using Newtonsoft.Json;

namespace Client.Services
{
    public class UsersService : IService<User>
    {
        private static readonly WebClient Client = new WebClient();

        private static string _path = "https://localhost:5001/api";

        public async Task<User> GetAsync(int id)
        {
            var str = await Client.DownloadStringTaskAsync(new Uri($"{_path}/Users/{id}"));
            return JsonConvert.DeserializeObject<User>(str);        }

        public async Task AddAsync(User entity)
        {
            await Utils.Request($"{_path}/Users", "POST", JsonConvert.SerializeObject(entity));
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            var str = await Client.DownloadStringTaskAsync(new Uri($"{_path}/Users"));
            return JsonConvert.DeserializeObject<IEnumerable<User>>(str);        }

        public async Task DeleteAsync(int id)
        {
            await Utils.Request($"{_path}/Users/{id}", "DELETE");
        }

        public async Task UpdateAsync(int id, User entity)
        {
            await Utils.Request($"{_path}/Users/{id}", "PUT", JsonConvert.SerializeObject(entity));
        }
    }
}