﻿using System;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        private static async Task MarkTask(int delay)
        {
            var id = await Requests.MarkRandomTaskWithDelay(delay);
            Console.WriteLine(id);
        }

        static int Main(string[] args)
        {
            MarkTask(1000);
            while (true)
            {
                Console.WriteLine(Console.ReadLine());
            }
            return 0;
        }
    }
}