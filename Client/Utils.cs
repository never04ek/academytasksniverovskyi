using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Models;
using Newtonsoft.Json;

namespace Client
{
    public class Utils
    {
        public static async Task<string> Request(string uri, string method, string data = null,
            string contentType = "application/json")
        {
            byte[] dataBytes = new byte[1];
            if (data != null)
                dataBytes = Encoding.UTF8.GetBytes(data);


            var request = (HttpWebRequest) WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            if (data != null)
                request.ContentLength = dataBytes.Length;
            request.ContentType = contentType;
            request.Method = method;
            if (data != null)
                using (var requestBody = await request.GetRequestStreamAsync())
                {
                    await requestBody.WriteAsync(dataBytes, 0, dataBytes.Length);
                }

            using (var response = await request.GetResponseAsync())
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream ?? throw new InvalidOperationException()))
            {
                return reader.ReadToEnd();
            }
        }
    }
}