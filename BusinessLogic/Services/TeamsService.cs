using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Repository;
using Models;

namespace BusinessLogic.Services
{
    public class TeamsService
    {
        private readonly TeamRepository _teamRepository;

        public TeamsService(TeamRepository teamRepository)
        {
            _teamRepository = teamRepository;
        }

        public async Task<IEnumerable<Team>> GetTeams()
        {
            return await _teamRepository.GetAllAsync();
        }

        public async Task<Team> GetTeam(int id)
        {
            return await _teamRepository.GetAsync(id);
        }

        public async Task AddTeam(Team team)
        {
            await _teamRepository.AddAsync(team);
        }

        public async Task UpdateTeam(int id, Team team)
        {
            team.Id = id;
            await _teamRepository.UpdateAsync(team);
        }

        public async Task DeleteTeam(int id)
        {
            await _teamRepository.DeleteAsync(_teamRepository.GetAllAsync().Result.FirstOrDefault(pr => pr.Id==id));
        }
    }
}