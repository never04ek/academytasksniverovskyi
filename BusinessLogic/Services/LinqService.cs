using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Models;
using Newtonsoft.Json;

namespace BusinessLogic.Services
{
    public class LinqService
    {
        private ProjectRepository _projectRepository;
        private TasksRepository _tasksRepository;
        private TeamRepository _teamRepository;
        private UserRepository _userRepository;

        private Dictionary<int, string> _tasks;

        public LinqService(UserRepository userRepository, TeamRepository teamRepository,
            TasksRepository tasksRepository, ProjectRepository projectRepository)
        {
            _userRepository = userRepository;
            _teamRepository = teamRepository;
            _tasksRepository = tasksRepository;
            _projectRepository = projectRepository;
            _tasks = new Dictionary<int, string>
            {
                {1, "Task1: GetAmountOfUserTasksInEachProject(id)"},
                {2, "Task2: GetUserTasks45Name(id)"},
                {3, "Task3: GetThisYearTasksOfUser(id)"},
                {4, "Task4: GetTeamsAndUsers()"},
                {5, "Task5: GetUserAndTasks()"},
                {6, "Task6: GetSpecUserStruct(id)"},
                {7, "Task7: GetSpecProjectStruct(id)"}
            };
        }

        #region Task1

        public async Task<Dictionary<Project, int>> GetAmountOfUserTasksInEachProject(int id)
        {
            var projects = (await _projectRepository.GetAllAsync()).ToList();
            return (await _tasksRepository.GetAllAsync())
                .Where(task => task.PerformerId == id)
                .GroupBy(task => task.ProjectId)
                .ToDictionary(group => projects[group.Key - 1], group => group.Count());
        }

        #endregion

        #region Task2

        public async Task<List<Tasks>> GetUserTasks45Name(int id)
        {
            return (await _tasksRepository.GetAllAsync())
                .Where(task => task.PerformerId == id && task.Name.Length < 45).ToList();
        }

        #endregion

        #region Task3

        public struct Task3Struct
        {
            public int Id;
            public string Name;
        }

        public async Task<List<Task3Struct>> GetThisYearTasksOfUser(int id)
        {
            return await _tasksRepository.GetAll()
                .Where(task => task.PerformerId == id)
                .Where(task => task.FinishedAt.Year == 2019)
                .Select(task => new Task3Struct {Id = task.Id, Name = task.Name})
                .ToListAsync();
        }

        #endregion

        #region Task4

        public struct Task4Struct
        {
            public int? Id;
            public string Name;
            public List<User> Users;
        }

        public async Task<IEnumerable<Task4Struct>> GetTeamsAndUsers()
        {
            //Displays nothing, because all teams have participants under 12 years old.
            var users = await _userRepository.GetAllAsync();
            var teams = await _teamRepository.GetAllAsync();
            return users
                .Where(user => user.TeamId != null)
                .GroupBy(user => user.TeamId)
                .Where(team =>
                    team.All(user => DateTime.Now.Year - user.Birthday.Year > 12))
                .Select(team => new Task4Struct
                {
                    Id = team.Key,
                    Name = teams.ToList()[(int) team.Key - 1].Name,
                    Users = team.OrderByDescending(user => user.TimeOfRegistration).ToList()
                });
        }

        #endregion

        #region Task5

        public struct Task5Struct
        {
            public User User;
            public List<Tasks> Tasks;
        }

        public async Task<IEnumerable<Task5Struct>> GetUserAndTasks()
        {
            var users = await _userRepository.GetAllAsync();
            return _tasksRepository.GetAllAsync().Result
                .GroupBy(task => task.PerformerId)
                .Select(userT => new Task5Struct
                {
                    User = users.ToList()[userT.Key - 1],
                    Tasks = userT.OrderByDescending(task => task.Name.Length).ToList()
                })
                .OrderBy(user => user.User.FirstName);
        }

        #endregion

        #region Task6

        public struct Task6Struct
        {
            public User User;
            public Project Project;
            public int LastTasks;
            public int NotFinishedTasks;
            public Tasks LongestTask;
        }


        public async Task<Task6Struct> GetSpecUserStruct(int id)
        {
            var user = await _userRepository.GetAsync(id);
            var projects = await _projectRepository.GetAllAsync();
            var tasks = await _tasksRepository.GetAllAsync();
            var res = projects
                .Where(project => project.AuthorId == user.Id)
                .Where(project =>
                    project.CreatedAt == projects
                        .Where(projectmin => projectmin.AuthorId == user.Id)
                        .Max(projectmin => projectmin.CreatedAt))
                .Select(project => new Task6Struct
                    {
                        User = user,
                        Project = project,
                        LastTasks = tasks.Count(task => project.Id == task.ProjectId),
                        NotFinishedTasks = tasks.Where(task => task.PerformerId == user.Id)
                            .Count(task => task.State == TaskState.Finished || task.State == TaskState.Canceled),
                        LongestTask = tasks.Where(task => task.PerformerId == user.Id)
                            .OrderBy(task => task.FinishedAt - task.CreatedAt).Last()
                    }
                ).FirstOrDefault();


            return res;
        }

        #endregion

        #region Task7

        public struct Task7Struct
        {
            public Project Project;
            public Tasks LongestTask;
            public Tasks ShortestTask;
            public int? UsersAmount;
        }

        public async Task<Task7Struct> GetSpecProjectStruct(int id)
        {
            var users = (await _userRepository.GetAllAsync()).ToList();
            var projects = (await _projectRepository.GetAllAsync()).ToList();
            var tasks = await _tasksRepository.GetAllAsync();
            return tasks.Where(task => task.ProjectId == id)
                .Where(task =>
                    task.DescriptionTask.Length == tasks.Where(taskd => taskd.ProjectId == id)
                        .Max(taskd => taskd.DescriptionTask.Length)).Select(task => new Task7Struct
                {
                    Project = projects[id - 1],
                    LongestTask = task,
                    ShortestTask = tasks.Where(taskn =>
                        taskn.ProjectId == id).FirstOrDefault(taskn =>
                        taskn.Name.Length == tasks.Where(tasknn => tasknn.ProjectId == id)
                            .Min(tasknn => tasknn.Name.Length)),
                    UsersAmount = users.Count(user => user.TeamId == projects[id - 1].TeamId &&
                                                      (projects[id - 1].Description.Length > 25 ||
                                                       tasks.Count(taskc => taskc.ProjectId == id) < 3)) == 0
                        ? null
                        : (int?) users.Count(user =>
                            user.TeamId == projects[id - 1].TeamId)
                }).FirstOrDefault();
        }

        #endregion

        public string GetListTasks()
        {
            return JsonConvert.SerializeObject(_tasks);
        }
        
    }
}