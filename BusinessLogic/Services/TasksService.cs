using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using DataAccess.Repository;
using Models;

namespace BusinessLogic.Services
{
    public class TasksService
    {
        private readonly TasksRepository _tasksRepository;

        public TasksService(TasksRepository tasksRepository)
        {
            _tasksRepository = tasksRepository;
        }

        public async Task<IEnumerable<Tasks>> GetTasks()
        {
            return await _tasksRepository.GetAllAsync();
        }

        public async Task<Tasks> GetTask(int id)
        {
            return await _tasksRepository.GetAsync(id);
        }

        public async Task AddTask(Tasks tasks)
        {
            await _tasksRepository.AddAsync(tasks);
        }

        public async Task UpdateTask(int id, Tasks tasks)
        {
            tasks.Id = id;
            await _tasksRepository.UpdateAsync(tasks);
        }

        public async Task DeleteTask(int id)
        { 
            await _tasksRepository.DeleteAsync(_tasksRepository.GetAllAsync().Result.FirstOrDefault(pr => pr.Id==id));
        }
    }
}