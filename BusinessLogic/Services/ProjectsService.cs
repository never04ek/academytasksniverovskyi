using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Repository;
using Models;

namespace BusinessLogic.Services
{
    public class ProjectsService
    {
        private readonly ProjectRepository _projectRepository;

        public ProjectsService(ProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public async Task<IEnumerable<Project>> GetProjects()
        {
            return await _projectRepository.GetAllAsync();
        }

        public async Task<Project> GetProject(int id)
        {
            return await _projectRepository.GetAsync(id);
        }

        public async Task AddProject(Project project)
        {
            await _projectRepository.AddAsync(project);
        }

        public async Task UpdateProject(int id, Project project)
        {
            project.Id = id;
            await _projectRepository.UpdateAsync(project);
        }

        public async Task DeleteProject(int id)
        { 
            await _projectRepository.DeleteAsync(_projectRepository.GetAllAsync().Result.FirstOrDefault(pr => pr.Id==id));
        }
    }
}