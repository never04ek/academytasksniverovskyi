using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Repository;
using Models;

namespace BusinessLogic.Services
{
    public class UsersService
    {
        private readonly UserRepository _userRepository;

        public UsersService(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _userRepository.GetAllAsync();
        }

        public async Task<User> GetUser(int id)
        {
            return await _userRepository.GetAsync(id);
        }

        public async Task AddUser(User user)
        {
            await _userRepository.AddAsync(user);
        }

        public async Task UpdateUser(int id, User user)
        {
            user.Id = id;
            await _userRepository.UpdateAsync(user);
        }

        public async Task DeleteUser(int id)
        {
            await _userRepository.DeleteAsync(_userRepository.GetAllAsync().Result.FirstOrDefault(pr => pr.Id==id));
        }
    }
}