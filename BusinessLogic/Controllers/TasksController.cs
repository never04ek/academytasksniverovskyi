using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace BusinessLogic.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TasksService _tasksService;

        public TasksController(TasksService tasksService)
        {
            _tasksService = tasksService;
        }

        // GET api/tasks
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            try
            {
                var tasks = await _tasksService.GetTasks();
                return Ok(tasks);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }


        // GET api/tasks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Tasks>> Get(int id)
        {
            try
            {
                var task = await _tasksService.GetTask(id);
                return Ok(task);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // POST api/tasks
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Tasks value)
        {
            try
            {
                await _tasksService.AddTask(value);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // PUT api/tasks/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] Tasks value)
        {
            try
            {
                await _tasksService.UpdateTask(id, value);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // DELETE api/tasks/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _tasksService.DeleteTask(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }
    }
}