using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace BusinessLogic.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamsService _teamsService;

        public TeamsController(TeamsService teamsService)
        {
            _teamsService = teamsService;
        }

        // GET api/teams
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Team>>> Get()
        {
            try
            {
                var teams = await _teamsService.GetTeams();
                return Ok(teams);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // GET api/teams/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Team>> Get(int id)
        {
            try
            {
                var team = await _teamsService.GetTeam(id);
                return Ok(team);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // POST api/teams
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Team value)
        {
            try
            {
                await _teamsService.AddTeam(value);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // PUT api/teams/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] Team value)
        {
            try
            {
                await _teamsService.UpdateTeam(id, value);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // DELETE api/teams/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _teamsService.DeleteTeam(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }
    }
}