using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Models;

namespace DataAccess.Repository
{
    public class UserRepository : Repository<User>
    {
        public UserRepository(AcademyDbContext dbContext) :
            base(dbContext)
        {
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await GetAll().OrderBy(x => x.Id)
                .ToListAsync();
        }

        public async Task AddAsync(User entity)
        {
            Add(entity);
            await SaveAsync();
        }

        public async Task DeleteAsync(User entity)
        {
            Delete(entity);
            await SaveAsync();
        }

        public async Task<User> GetAsync(int id)
        {
            return await Get(pr => pr.Id.Equals(id)).FirstOrDefaultAsync();
        }

        public async Task UpdateAsync(User entity)
        {
            Update(entity);
            await SaveAsync();
        }
    }
}