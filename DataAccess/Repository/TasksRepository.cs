using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Models;

namespace DataAccess.Repository
{
    public class TasksRepository : Repository<Tasks>
    {
        public TasksRepository(AcademyDbContext dbContext) :
            base(dbContext)
        {
        }

        public async Task<IEnumerable<Tasks>> GetAllAsync()
        {
            return await GetAll().OrderBy(x => x.Id)
                .ToListAsync();
        }

        public async Task AddAsync(Tasks entity)
        {
            Add(entity);
            await SaveAsync();
        }

        public async Task DeleteAsync(Tasks entity)
        {
            Delete(entity);
            await SaveAsync();
        }

        public async Task<Tasks> GetAsync(int id)
        {
            return await Get(pr => pr.Id.Equals(id)).FirstOrDefaultAsync();
        }

        public async Task UpdateAsync(Tasks entity)
        {
            Update(entity);
            await SaveAsync();
        }
    }
}