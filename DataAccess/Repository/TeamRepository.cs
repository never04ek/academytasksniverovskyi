using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Models;

namespace DataAccess.Repository
{
    public class TeamRepository : Repository<Team>
    {
        public TeamRepository(AcademyDbContext dbContext) :
            base(dbContext)
        {
        }

        public async Task<IEnumerable<Team>> GetAllAsync()
        {
            return await GetAll().OrderBy(x => x.Id)
                .ToListAsync();
        }

        public async Task AddAsync(Team entity)
        {
            Add(entity);
            await SaveAsync();
        }

        public async Task DeleteAsync(Team entity)
        {
            Delete(entity);
            await SaveAsync();
        }

        public async Task<Team> GetAsync(int id)
        {
            return await Get(pr => pr.Id.Equals(id)).FirstOrDefaultAsync();
        }

        public async Task UpdateAsync(Team entity)
        {
            Update(entity);
            await SaveAsync();
        }
    }
}