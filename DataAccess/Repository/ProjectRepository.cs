using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Models;

namespace DataAccess.Repository
{
    public class ProjectRepository : Repository<Project>, IRepository<Project>
    {
        public ProjectRepository(AcademyDbContext dbContext) :
            base(dbContext)
        {
        }

        public async Task<IEnumerable<Project>> GetAllAsync()
        {
            return await GetAll().OrderBy(x => x.Id)
                .ToListAsync();
        }

        public async Task AddAsync(Project entity)
        {
            Add(entity);
            await SaveAsync();
        }

        public async Task DeleteAsync(Project entity)
        {
            Delete(entity);
            await SaveAsync();
        }

        public async Task<Project> GetAsync(int id)
        {
            return await Get(pr => pr.Id.Equals(id)).FirstOrDefaultAsync();
        }

        public async Task UpdateAsync(Project entity)
        {
            Update(entity);
            await SaveAsync();
        }
    }
}