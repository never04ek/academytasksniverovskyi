using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {
        protected AcademyDbContext DbContext { get; set; }

        protected Repository(AcademyDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public IQueryable<T> GetAll()
        {
            return DbContext.Set<T>();
        }

        public void Add(T entity)
        {
            DbContext.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            DbContext.Set<T>().Remove(entity);
        }


        public IQueryable<T> Get(Expression<Func<T, bool>> expression)
        {
            return DbContext.Set<T>()
                .Where(expression);
        }

        public void Update(T entity)
        {
            DbContext.Set<T>().Update(entity);
        }

        public async Task SaveAsync()
        {
            await DbContext.SaveChangesAsync();
        }
    }
}