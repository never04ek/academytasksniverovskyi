using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        void Add(T entity);
        void Delete(T entity);
        IQueryable<T> Get(Expression<Func<T, bool>> expression);
        void Update(T entity);
        Task SaveAsync();
    }
}